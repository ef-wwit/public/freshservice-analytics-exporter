[![pipeline status](https://gitlab.com/ef-wwit/public/freshservice-analytics-exporter/badges/master/pipeline.svg)](https://gitlab.com/ef-wwit/public/freshservice-analytics-exporter/-/commits/master)

### FreshService Analytics Exporter
Exports CSV data from freshservice analytics API, converts it to JSON and ships it to elasticsearch.

#### Features
- Automated CRON that runs the export at minute 0 of every hour (Europe/London Timezone).
- Initial export is done on startup.
- Automatically convert date fields to correct format for elasticsearch.

#### Dockerfile
```
# Base image and packages.
FROM    alpine:3.6
RUN     apk add --no-cache nodejs nodejs-npm

# Set work directory and add required application files.
WORKDIR /root
ADD app/ /root

# Install dependencies.
RUN npm install

# Launche the application using node as an entrypoint.
ENTRYPOINT ["node", "app.js"]
```

#### Required Enviroment Variables
- **FreshService**
    - `API_ENDPOINT_URL`: API URL.
    - `API_USERNAME`: API user username.
    - `API_PASSWORD`: API user password.
- **Elasticsearch**
    - `ES_ENDPOINT_URL`: URL and port (***https://localhost:9200***).
    - `ES_USERNAME`: Username.
    - `ES_PASSWORD`: Password.
    - `ES_INDEX`: Index where the data should be stored.