# Base image and packages.
FROM    alpine:3.6
RUN     apk add --no-cache nodejs nodejs-npm

# Set work directory and add required application files.
WORKDIR /root
ADD app/ /root

# Install dependencies.
RUN npm install

# Launche the application using node as an entrypoint.
ENTRYPOINT ["node", "app.js"]