const request = require('request');
const process = require('process');

const API_ENDPOINT_URL = process.env["API_ENDPOINT_URL"];
const API_USERNAME = process.env["API_USERNAME"];
const API_PASSWORD = process.env["API_PASSWORD"];

function csvToArray(text) {
    var re_valid = /^\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*(?:,\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*)*$/;
    var re_value = /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,|$)/g;
    // Return NULL if input string is not well formed CSV string.
    if (!re_valid.test(text)) return null;
    var a = [];                     // Initialize array to receive values.
    text.replace(re_value, // "Walk" the string using replace with callback.
        function(m0, m1, m2, m3) {
            // Remove backslash from \' in single quoted values.
            if      (m1 !== undefined) a.push(m1.replace(/\\'/g, "'"));
            // Remove backslash from \" in double quoted values.
            else if (m2 !== undefined) a.push(m2.replace(/\\"/g, '"'));
            else if (m3 !== undefined) a.push(m3);
            return ''; // Return empty string.
        });
    // Handle special case of empty last value.
    if (/,\s*$/.test(text)) a.push('');
    return a;
};

const callAPI = (callback) => {
    console.log(`Calling '${API_ENDPOINT_URL}'`);
    request.get(API_ENDPOINT_URL, {
        headers: {
            Authorization: `Basic ${Buffer.from(`${API_USERNAME}:${API_PASSWORD}`).toString('base64')}`
        }
    }, (err, resp, body) => {
        callback(body);
    });
}

module.exports = {
    getFreshAnalytics: (callback) => {
        var headers = [];
        var jsonData = {
            items: []
        };
        callAPI((csvData) => {
            var csvLines = csvData.split("\n");
            for (var header of csvLines[0].split(",")) {
                headers.push(`${header.split("\"").join("").trim()}`);
            }
            csvLines.splice(0, 1);
            
            for (var csvLine of csvLines) {
                var jsonObject = {};
                var lineItems = csvToArray(csvLine);
                if (lineItems) {
                    var iter = 0;
                    for (var lineItem of lineItems) {
                        lineItem = `${lineItem.split("\"").join("").trim()}`;

                        // Try and convert date fields.
                        if (headers[iter].toLowerCase().includes("date")) {
                            var defaultValue = lineItems[headers.indexOf("Created Date")];
                            try {
                                lineItem = new Date(lineItem).toISOString();
                            }catch (err) {
                                // Do Nothing.
                                lineItem = defaultValue;
                            }
                        }

                        // Try and convert integer fields.
                        if (headers[iter].toLowerCase().includes("count")) {
                            var defaultValue = 0;
                            try {
                                lineItem = parseInt(lineItem.split(",").join(""));
                            }catch (err) {
                                // Do Nothing.
                                lineItem = defaultValue;
                            }
                        }

                        // Try and convert float fields.
                        if (headers[iter].toLowerCase().includes("time")) {
                            var defaultValue = 0;
                            try {
                                lineItem = parseFloat(lineItem.split(",").join(""));
                            }catch (err) {
                                // Do Nothing.
                                lineItem = defaultValue;
                            }
                        }

                        jsonObject[headers[iter]] = lineItem;
                        
                        iter ++;
                    }
                    jsonData.items.push(jsonObject);
                }            
            }
            callback(jsonData);
        });
    }
}