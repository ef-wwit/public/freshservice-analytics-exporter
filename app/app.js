const api = require("./api-handler")
const elastic = require("./elasticsearch-exporter")
var Cron = require('cron').CronJob;

// Exporter function.
const runExport = (callback = null) => {
    api.getFreshAnalytics((jsonData) => {
        console.log(`Total Entries: ${jsonData.items.length}`);
        elastic.exportData(jsonData.items, () => {
            console.log("Data export complete.");
            callback();
        })
    });
}

// Confugure conr job.
var job = new Cron('0 * * * *', function() {
    job.running ? setTimeout(() => {
        console.log("Job is already running, waiting 5 minutes.")
    }, 1000 * 60 * 5) : console.log('Running scheduled data export.'); runExport();
}, null, true, 'Europe/London');

// Run the export at startup.
runExport (() => {
    job.start();
})
