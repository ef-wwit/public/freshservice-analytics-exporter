const request = require('request');
const process = require('process');

const ES_ENDPOINT_URL = process.env["ES_ENDPOINT_URL"];
const ES_INDEX = process.env["ES_INDEX"];
const ES_USERNAME = process.env["ES_USERNAME"];
const ES_PASSWORD = process.env["ES_PASSWORD"];
const ES_URL = `${ES_ENDPOINT_URL}/${ES_INDEX}/_doc`;


const esAPICall = (object, callback) => {
    console.log(`Processing entry ${object["ID"]}`);
    request.put({
        url: `${ES_URL}/${object["ID"]}`,
        headers: {
            Authorization: `Basic ${Buffer.from(`${ES_USERNAME}:${ES_PASSWORD}`).toString('base64')}`
        },
        json: object,

    }, (err, resp, body) => {
        err ? console.log(err) : null;
        callback();
    });
}

class callbackFunction extends Function {}

const exportItems = (items = [], i = 0, callback = null) => {
    i < items.length ? 
        esAPICall(items[i], () => {i++; exportItems(items, i)}) : 
        callback? callback() : null;
}

module.exports = {
    /**
     * @function exportData
     * Send each item in the data array "items" to elasticsearch as individual documents.
     * The entry "ID" field is used as a document identifier to prevent duplication.
     * @param items
     * Array of JSON items to send.
     */
    exportData: (items = [], callback) => {
        exportItems(items, 0, () => {
            callback();
        });
    }
}
