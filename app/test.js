const api = require("./api-handler")
const elastic = require("./elasticsearch-exporter")

const runExport = () => {
    api.getFreshAnalytics((jsonData) => {
        elastic.exportData(jsonData.items, () => {
            console.log("Data export complete.");
        })
    });
}

runExport();